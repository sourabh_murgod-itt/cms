def user_portal():
    user_nm = uunn

    def order_history():
        top = Tk()
        top.geometry("400x400+400+150")
        top.title("Order History")

        label34 = Label(top, text="SLNO", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=10, y=10)
        my_cursor.execute("select * from user_orderp_" + user_nm)
        r = my_cursor
        yyy = 40
        for i in r:
            if i[0] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[0], width=11, font=('Arial', 12), bg="white")
                label34.place(x=10, y=yyy)
                yyy += 24

        label34 = Label(top, text="No. Items", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=110, y=10)
        my_cursor.execute("select * from user_orderp_" + user_nm)
        r = my_cursor
        yyy = 40
        for i in r:
            if i[1] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[1], width=11, font=('Arial', 12), bg="white")
                label34.place(x=110, y=yyy)
                yyy += 24

        label34 = Label(top, text="Time", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=210, y=10)
        my_cursor.execute("select * from user_orderp_" + user_nm)
        r = my_cursor
        yyy = 40
        for i in r:
            if i[2] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[2], width=11, font=('Arial', 12), bg="white")
                label34.place(x=210, y=yyy)
                yyy += 24

        label34 = Label(top, text="Amount", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=310, y=10)
        my_cursor.execute("select * from user_orderp_" + user_nm)
        r = my_cursor
        yyy = 40
        for i in r:
            if i[3] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[3], width=11, font=('Arial', 12), bg="white")
                label34.place(x=310, y=yyy)
                yyy += 24

        top.mainloop()

    def place_order():
        my_cursor.execute(
            "create table if not exists user_orderp_" + user_nm + " (slno int NOT NULL AUTO_INCREMENT primary key,item_no int,time int,total int)")
        my_cursor.execute(
            "create table if not exists admin_orderp_ (slno int NOT NULL AUTO_INCREMENT primary key,user_name varchar(100),item_no int,time int,total int)")
        my_cursor.execute(
            "insert into user_orderp_" + user_nm + " (item_no,time,total) values(" + str(cnt) + "," + str(
                tttt) + "," + str(mmmm) + ");")
        my_cursor.execute(
            "insert into admin_orderp_ (user_name,item_no,time,total) values(" + '"' + user_nm + '",' + str(
                cnt) + "," + str(tttt) + "," + str(mmmm) + ");")
        my_cursor.execute(
            "delete from user_order_" + user_nm + ";")
        messagebox.showinfo('Success', 'Hi! Your order is successfully placed.')
        mydb.commit()
        mop.destroy()
        user_portal()
        # messagebox.showinfo('Success', 'Hi! Your order is successfully placed.')

    def add_item():
        user_nm = uunn

        def add_new_item_submit():
            user_nm = uunn
            i_name = options1.get()

            i_price = entry_add_item_price.get()  # this is quantity
            # i_time = entry_add_item_time.get()

            my_cursor.execute("select * from menu_dummy")
            r = my_cursor
            item_price = 0
            for i in r:
                if (i[1] == i_name):
                    i_time = i[3]
                    item_price = i[2]  # item_price

            tot = int(i_price) * (item_price)

            if (len(i_name) == "DROP" or len(i_price) == 0):
                messagebox.showerror("Invalid", "Empty Fields")
            else:
                my_cursor.execute(
                    "create table if not exists user_order_" + user_nm + " (slno int NOT NULL AUTO_INCREMENT primary key,item_name varchar(100),quantity int,price int,time int,total int)")
                my_cursor.execute(
                    "insert into user_order_" + user_nm + " (item_name,quantity,price,time,total) values(" + '"' + i_name + '"' + "," + str(
                        i_price) + ',' + str(item_price) + ',' + str(i_time) + ',' + str(tot) + " );")
                mydb.commit()
                master.destroy()
                mop.destroy()
                user_portal()

        master = Tk()
        master.geometry("350x160+500+200")
        master.title("Add Item")

        label_add_item_name = Label(master, text="Item Name:", font=('calibri', 15), width=15, anchor=W)
        label_add_item_name.place(x=10, y=10)

        my_cursor.execute("select * from menu_dummy")
        r = my_cursor
        list = []
        for i in r:
            if i[1] == None:
                abx = 0;
            else:
                list.append(i[1])
        options1 = StringVar(master)
        options1.set("DROP")
        om2 = OptionMenu(master, options1, *list)
        om2.place(x=140, y=10)

        # entry_add_item_name = Entry(master, width=20, font=('calibri', 15), fg="black")
        # entry_add_item_name.place(x=140, y=10)

        label_add_item_price = Label(master, text="Quantity:", font=('calibri', 15), width=15, anchor=W)
        label_add_item_price.place(x=10, y=50)
        entry_add_item_price = Entry(master, width=20, font=('calibri', 15), fg="black")
        entry_add_item_price.place(x=140, y=50)

        # label_add_item_time = Label(master, text="Prep Time(m):", font=('calibri', 15), width=15, anchor=W)
        # label_add_item_time.place(x=10, y=90)
        # entry_add_item_time = Entry(master, width=20, font=('calibri', 15), fg="black")
        # entry_add_item_time.place(x=140, y=90)

        button_submit_new_item = Button(master, text="Update Item", bg="grey", width=15, height=1,
                                        font=('comicsans', 15),
                                        command=add_new_item_submit)
        button_submit_new_item.place(x=90, y=100)

        master.mainloop()

    def updt():
        def update_new_item_submit():
            user_nm = uunn
            i_name = options1.get()

            if (len(i_name) == "DROP"):
                messagebox.showerror("Invalid", "Empty Fields")
            else:
                my_cursor.execute("DELETE FROM user_order_" + user_nm + " WHERE item_name='" + i_name + "';")
                mydb.commit()
                master.destroy()
                mop.destroy()
                user_portal()

        master = Tk()
        master.geometry("350x100+500+200")
        master.title("Add Item")

        label_add_item_name = Label(master, text="Item Name:", font=('calibri', 15), width=15, anchor=W)
        label_add_item_name.place(x=10, y=10)

        my_cursor.execute("select * from user_order_" + user_nm)
        r = my_cursor
        list = []
        for i in r:
            if i[1] == None:
                abx = 0;
            else:
                list.append(i[1])
        options1 = StringVar(master)
        options1.set("DROP")
        om2 = OptionMenu(master, options1, *list)
        om2.place(x=140, y=10)

        # entry_add_item_name = Entry(master, width=20, font=('calibri', 15), fg="black")
        # entry_add_item_name.place(x=140, y=10)

        # label_add_item_price = Label(master, text="Item Price:", font=('calibri', 15), width=15, anchor=W)
        # label_add_item_price.place(x=10, y=50)
        # entry_add_item_price = Entry(master, width=20, font=('calibri', 15), fg="black")
        # entry_add_item_price.place(x=140, y=50)

        # label_add_item_time = Label(master, text="Prep Time(m):", font=('calibri', 15), width=15, anchor=W)
        # label_add_item_time.place(x=10, y=90)
        # entry_add_item_time = Entry(master, width=20, font=('calibri', 15), fg="black")
        # entry_add_item_time.place(x=140, y=90)

        button_submit_new_item = Button(master, text="Delete Item", bg="grey", width=15, height=1,
                                        font=('comicsans', 15),
                                        command=update_new_item_submit)
        button_submit_new_item.place(x=90, y=50)

        master.mainloop()

    mop = Tk()
    mop.geometry("1340x680+10+10")
    mop.title("MENU")
    filename = PhotoImage(file="login_wall1_0.5.png")
    background_label = Label(mop, image=filename, anchor=W)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)

    button_add_item = Button(mop, text="Add Item", width=15, height=1, font=('comicsans', 18), command=add_item)
    button_add_item.place(x=1060, y=540)

    button_update_item = Button(mop, text="Delete Item", width=15, height=1, font=('comicsans', 18),
                                command=updt)
    button_update_item.place(x=1060, y=600)

    label34 = Label(mop, text="ITEM", width=19, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=20, y=46)
    my_cursor.execute("select * from menu_dummy")
    r = my_cursor
    yyy = 70
    for i in r:
        if i[1] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[1], width=19, font=('Arial', 12), bg="white", anchor=W)
            label34.place(x=20, y=yyy)
            yyy += 24

    label34 = Label(mop, text="PRICE", width=12, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=180, y=46)
    my_cursor.execute("select * from menu_dummy")
    r = my_cursor
    yyy = 70
    for i in r:
        if i[2] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[2], width=10, font=('Arial', 12), bg="white", anchor=W)
            label34.place(x=180, y=yyy)
            yyy += 24

    label34 = Label(mop, text="TIME", width=8, font=('Arial', 14), bg="white", anchor=E)
    label34.place(x=250, y=46)
    my_cursor.execute("select * from menu_dummy")
    r = my_cursor
    yyy = 70
    for i in r:
        if i[2] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[2], width=10, font=('Arial', 12), bg="white", anchor=E)
            label34.place(x=250, y=yyy)
            yyy += 24

    # code till this was for the display of the menu
    # code below is for the cart

    label34 = Label(mop, text="SLNO", width=9, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=520, y=46)
    my_cursor.execute("select * from user_order_" + user_nm)
    r = my_cursor
    yyy = 70
    for i in r:
        if i[0] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[0], width=11, font=('Arial', 12), bg="white")
            label34.place(x=520, y=yyy)
            yyy += 24

    label34 = Label(mop, text="I NAME", width=9, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=620, y=46)
    my_cursor.execute("select * from user_order_" + user_nm)
    r = my_cursor
    yyy = 70
    for i in r:
        if i[1] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[1], width=11, font=('Arial', 12), bg="white")
            label34.place(x=620, y=yyy)
            yyy += 24

    label34 = Label(mop, text="QUANTITY", width=9, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=720, y=46)
    my_cursor.execute("select * from user_order_" + user_nm)
    r = my_cursor
    yyy = 70
    for i in r:
        if i[2] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[2], width=11, font=('Arial', 12), bg="white")
            label34.place(x=720, y=yyy)
            yyy += 24

    label34 = Label(mop, text="PRICE", width=9, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=820, y=46)
    my_cursor.execute("select * from user_order_" + user_nm)
    r = my_cursor
    yyy = 70
    for i in r:
        if i[3] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[3], width=11, font=('Arial', 12), bg="white")
            label34.place(x=820, y=yyy)
            yyy += 24

    tttt = 0
    label34 = Label(mop, text="TIME", width=9, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=920, y=46)
    my_cursor.execute("select * from user_order_" + user_nm)
    r = my_cursor
    yyy = 70
    for i in r:
        if i[4] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[4], width=11, font=('Arial', 12), bg="white")
            label34.place(x=920, y=yyy)
            yyy += 24
            tttt = tttt + i[4]

    mmmm = 0
    label34 = Label(mop, text="TOTAL", width=9, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=1000, y=46)
    my_cursor.execute("select * from user_order_" + user_nm)
    r = my_cursor
    yyy = 70
    cnt = 0
    for i in r:
        if i[5] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[5], width=11, font=('Arial', 12), bg="white")
            label34.place(x=1000, y=yyy)
            yyy += 24
            mmmm = mmmm + i[5]
            cnt = cnt + 1

    label35 = Label(mop, text="₹" + str(mmmm), width=8, bg="green", font=('Arial', 18), anchor=W)
    label35.place(x=900, y=615)

    label35 = Label(mop, text=str(tttt) + "min", width=8, bg="green", font=('Arial', 18), anchor=W)
    label35.place(x=900, y=550)

    button_history = Button(mop, text="History", width=15, height=1, font=('comicsans', 18),
                            command=order_history)
    button_history.place(x=500, y=540)

    button_place_order = Button(mop, text="Place Order", width=15, height=1, font=('comicsans', 18),
                                command=place_order)
    button_place_order.place(x=500, y=600)

    mop.mainloop()