import mysql.connector
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk

mydb = mysql.connector.connect(host="localhost", user="root", password="1234", database="abc")
my_cursor = mydb.cursor()


def admin_portal():
    user_nm = uunn

    def delete_item():
        def mnu_del():
            mkl = options1.get()
            my_cursor.execute("delete from menu_dummy where item_name='" + mkl + "';")
            mydb.commit()
            cop.destroy()
            mop.destroy()
            admin_portal()

        cop = Tk()
        cop.geometry("250x150+400+200")
        cop.title("Delete Item")
        my_cursor.execute("select * from menu_dummy")
        r = my_cursor
        list = []
        for i in r:
            if i[1] == None:
                abx = 0;
            else:
                list.append(i[1])
        options1 = StringVar(cop)
        options1.set("DROP")
        om2 = OptionMenu(cop, options1, *list)
        om2.place(x=30, y=30)

        button_del = Button(cop, text="Delete Item", bg="grey", width=10, height=1, font=('comicsans', 12),
                            command=mnu_del)
        button_del.place(x=50, y=80)

        cop.mainloop()

    def order_history():
        top = Tk()
        top.geometry("500x400+400+150")
        top.title("Order History")

        label34 = Label(top, text="SLNO", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=10, y=10)
        my_cursor.execute("select * from admin_orderp_")
        r = my_cursor
        yyy = 40
        for i in r:
            if i[0] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[0], width=11, font=('Arial', 12), bg="white")
                label34.place(x=10, y=yyy)
                yyy += 24

        label34 = Label(top, text="User", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=110, y=10)
        my_cursor.execute("select * from admin_orderp_")
        r = my_cursor
        yyy = 40
        for i in r:
            if i[1] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[1], width=11, font=('Arial', 12), bg="white")
                label34.place(x=110, y=yyy)
                yyy += 24

        label34 = Label(top, text="No. Items", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=210, y=10)
        my_cursor.execute("select * from admin_orderp_")
        r = my_cursor
        yyy = 40
        for i in r:
            if i[2] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[2], width=11, font=('Arial', 12), bg="white")
                label34.place(x=210, y=yyy)
                yyy += 24

        label34 = Label(top, text="Time", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=310, y=10)
        my_cursor.execute("select * from admin_orderp_")
        r = my_cursor
        yyy = 40
        for i in r:
            if i[3] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[3], width=11, font=('Arial', 12), bg="white")
                label34.place(x=310, y=yyy)
                yyy += 24

        label34 = Label(top, text="Amount", width=9, font=('Arial', 14), bg="white", anchor=W)
        label34.place(x=410, y=10)
        my_cursor.execute("select * from admin_orderp_")
        r = my_cursor
        yyy = 40
        for i in r:
            if i[4] == None:
                zzz = 0
            else:
                label34 = Label(top, text=i[4], width=11, font=('Arial', 12), bg="white")
                label34.place(x=410, y=yyy)
                yyy += 24

        top.mainloop()

    def add_item():
        def add_new_item_submit():
            aaaa = entry_add_item_time.get()
            print(aaaa)
            i_name = entry_add_item_name.get()
            i_price = entry_add_item_price.get()
            i_time = aaaa
            if (len(i_name) == 0 or len(i_price) == 0 or len(i_time) == 0):
                messagebox.showerror("Invalid", "Empty Fields")
            else:
                my_cursor.execute(
                    "create table if not exists menu_dummy (slno int NOT NULL AUTO_INCREMENT primary key,item_name varchar(100),price int,time int)")
                my_cursor.execute(
                    "insert into menu_dummy(item_name,price,time) values(" + '"' + i_name + '"' + "," + str(
                        i_price) + ',' + str(i_time) + " );")
                mydb.commit()
                master.destroy()
                mop.destroy()
                admin_portal()

        master = Tk()
        master.geometry("350x200+500+200")
        master.title("Add Item")

        label_add_item_name = Label(master, text="Item Name:", font=('calibri', 15), width=15, anchor=W)
        label_add_item_name.place(x=10, y=10)
        entry_add_item_name = Entry(master, width=20, font=('calibri', 15), fg="black")
        entry_add_item_name.place(x=140, y=10)

        label_add_item_price = Label(master, text="Item Price:", font=('calibri', 15), width=15, anchor=W)
        label_add_item_price.place(x=10, y=50)
        entry_add_item_price = Entry(master, width=20, font=('calibri', 15), fg="black")
        entry_add_item_price.place(x=140, y=50)

        label_add_item_time = Label(master, text="Prep Time(m):", font=('calibri', 15), width=15, anchor=W)
        label_add_item_time.place(x=10, y=90)
        entry_add_item_time = Entry(master, width=20, font=('calibri', 15), fg="black")
        entry_add_item_time.place(x=140, y=90)

        button_submit_new_item = Button(master, text="Update Item", bg="grey", width=15, height=1,
                                        font=('comicsans', 15),
                                        command=add_new_item_submit)
        button_submit_new_item.place(x=90, y=140)

        master.mainloop()

    def update_new_item_submit():
        i_name = options1.get()
        i_price = entry_add_item_price.get()
        i_time = entry_add_item_time.get()
        if (len(i_name) == "DROP" or len(i_price) == 0 or len(i_time) == 0):
            messagebox.showerror("Invalid", "Empty Fields")
        else:
            my_cursor.execute(
                "UPDATE menu_dummy SET price=" + i_price + ", time=" + i_time + " WHERE item_name=" + '"' + i_name + '";')
            mydb.commit()
            master.destroy()

    def updt():
        def update_new_item_submit():
            i_name = options1.get()
            i_price = entry_add_item_price.get()
            i_time = entry_add_item_time.get()
            if (len(i_name) == "DROP" or len(i_price) == 0 or len(i_time) == 0):
                messagebox.showerror("Invalid", "Empty Fields")
            else:
                my_cursor.execute(
                    "UPDATE menu_dummy SET price=" + i_price + ", time=" + i_time + " WHERE item_name=" + '"' + i_name + '";')
                mydb.commit()
                master.destroy()
                mop.destroy()
                admin_portal()

        master = Tk()
        master.geometry("350x200+500+200")
        master.title("Add Item")

        label_add_item_name = Label(master, text="Item Name:", font=('calibri', 15), width=15, anchor=W)
        label_add_item_name.place(x=10, y=10)

        my_cursor.execute("select * from menu_dummy")
        r = my_cursor
        list = []
        for i in r:
            if i[1] == None:
                abx = 0;
            else:
                list.append(i[1])
        options1 = StringVar(master)
        options1.set("DROP")
        om2 = OptionMenu(master, options1, *list)
        om2.place(x=140, y=10)

        # entry_add_item_name = Entry(master, width=20, font=('calibri', 15), fg="black")
        # entry_add_item_name.place(x=140, y=10)

        label_add_item_price = Label(master, text="Item Price:", font=('calibri', 15), width=15, anchor=W)
        label_add_item_price.place(x=10, y=50)
        entry_add_item_price = Entry(master, width=20, font=('calibri', 15), fg="black")
        entry_add_item_price.place(x=140, y=50)

        label_add_item_time = Label(master, text="Prep Time(m):", font=('calibri', 15), width=15, anchor=W)
        label_add_item_time.place(x=10, y=90)
        entry_add_item_time = Entry(master, width=20, font=('calibri', 15), fg="black")
        entry_add_item_time.place(x=140, y=90)

        button_submit_new_item = Button(master, text="Update Item", bg="grey", width=15, height=1,
                                        font=('comicsans', 15),
                                        command=update_new_item_submit)
        button_submit_new_item.place(x=90, y=140)

        master.mainloop()

    mop = Tk()
    mop.geometry("1340x680+10+10")
    mop.title("ADMIN")
    filename = PhotoImage(file="login_wall1_0.6.png")
    background_label = Label(mop, image=filename, anchor=W)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)

    button_add_item = Button(mop, text="Add Item", width=15, height=1, font=('comicsans', 18), command=add_item)
    button_add_item.place(x=1060, y=60)

    button_update_item = Button(mop, text="Update Item", width=15, height=1, font=('comicsans', 18),
                                command=updt)
    button_update_item.place(x=1060, y=140)

    button_history = Button(mop, text="History", width=15, height=1, font=('comicsans', 18),
                            command=order_history)
    button_history.place(x=1060, y=220)

    button_delete = Button(mop, text="Delete Item", width=15, height=1, font=('comicsans', 18),
                           command=delete_item)
    button_delete.place(x=1060, y=300)

    label34 = Label(mop, text="ITEM", width=19, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=20, y=46)
    my_cursor.execute("select * from menu_dummy")
    r = my_cursor
    yyy = 70
    for i in r:
        if i[1] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[1], width=19, font=('Arial', 12), bg="white", anchor=W)
            label34.place(x=20, y=yyy)
            yyy += 24

    label34 = Label(mop, text="PRICE", width=12, font=('Arial', 14), bg="white", anchor=W)
    label34.place(x=180, y=46)
    my_cursor.execute("select * from menu_dummy")
    r = my_cursor
    yyy = 70
    for i in r:
        if i[2] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[2], width=10, font=('Arial', 12), bg="white", anchor=W)
            label34.place(x=180, y=yyy)
            yyy += 24

    label34 = Label(mop, text="TIME", width=8, font=('Arial', 14), bg="white", anchor=E)
    label34.place(x=250, y=46)
    my_cursor.execute("select * from menu_dummy")
    r = my_cursor
    yyy = 70
    for i in r:
        if i[3] == None:
            zzz = 0
        else:
            label34 = Label(mop, text=i[3], width=10, font=('Arial', 12), bg="white", anchor=E)
            label34.place(x=250, y=yyy)
            yyy += 24

    mop.mainloop()