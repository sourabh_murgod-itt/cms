import mysql.connector
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk

mydb = mysql.connector.connect(host="localhost", user="root", password="1234", database="abc")
my_cursor = mydb.cursor()

def main():
    root=Tk()
    root.geometry("1340x680+10+10")
    root.title("LOG IN")

    filename = PhotoImage(file="login_wall1_0.4.png")
    background_label = Label(root, image=filename,anchor=W)
    background_label.place(x=0, y=0, relwidth=1, relheight=1)


    #C = Canvas(root, bg='light grey', height=300, width=500)


    #C.place(x=800,y=300)
    #root.wm_attributes('-transparentcolor', '#ab23ff')
    l_e1 = Label(root, text="Username",bg="light grey",font=('calibri', 20), width=15, anchor=W)
    l_e1.place(x=850, y=330)
    un_e1=Entry(root,width=29,font=('calibri', 20),fg="black")
    un_e1.place(x=850,y=380)

    l_p1 = Label(root, text="Password", bg="light grey", font=('calibri', 20), width=15, anchor=W)
    l_p1.place(x=850, y=430)
    un_p1 = Entry(root, width=29,show="*", font=('calibri', 20), fg="black")
    un_p1.place(x=850, y=480)

    button_login = Button(root, text="Log In", width=15, height=1, font=('comicsans', 14),command=login)
    button_login.place(x=850, y=540)
    button_signup = Button(root, text="Sign Up", width=15, height=1, font=('comicsans', 14),command=sign_up)
    button_signup.place(x=1070, y=540)


    root.mainloop()

main()