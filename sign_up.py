import mysql.connector
from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk

mydb = mysql.connector.connect(host="localhost", user="root", password="1234", database="abc")
my_cursor = mydb.cursor()

def sign_up():
    def cheeck():

        r_email = r_email_e.get()
        r_pass = r_password_e.get()
        r_cpass = r_cpassword_e.get()
        r_ph = r_ph_e.get()
        if (r_pass != r_cpass):
            messagebox.showerror("Invalid", "Passwords doesn't Match")
        elif (len(r_ph) != 10):
            messagebox.showerror("Invalid", "Invalid Phone")
        elif (len(r_email) == 0 or len(r_pass) == 0 or len(r_cpass) == 0 or len(r_ph) == 0):
            messagebox.showerror("Invalid", "Invalid Credentials")
        else:
            my_cursor.execute(
                "create table if not exists users_accounts (Personid int NOT NULL AUTO_INCREMENT primary key,username varchar(100),password varchar(100),phone bigint)")
            my_cursor.execute(
                "insert into users_accounts(username,password,phone) values(" + '"' + r_email + '"' + "," + '"' + r_pass + '",' + str(
                    r_ph) + " );")
            mydb.commit()
            top.destroy()

    top = Tk()
    top.geometry("640x340+350+100")
    top.title("SIGN UP")

    C1 = Canvas(top, bg='light grey', height=500, width=700)
    C1.place(x=0, y=0)

    r_email_l = Label(top, text="Username:", bg="light grey", font=('calibri', 20), width=15, anchor=W)
    r_email_l.place(x=30, y=30)
    r_email_e = Entry(top, width=29, font=('calibri', 20), fg="black")
    r_email_e.place(x=170, y=30)

    r_password_l = Label(top, text="Password:", bg="light grey", font=('calibri', 20), width=15, anchor=W)
    r_password_l.place(x=30, y=80)
    r_password_e = Entry(top, width=29, show="*", font=('calibri', 20), fg="black")
    r_password_e.place(x=170, y=80)

    r_cpassword_l = Label(top, text="Confirm:", bg="light grey", font=('calibri', 20), width=15, anchor=W)
    r_cpassword_l.place(x=30, y=130)
    r_cpassword_e = Entry(top, width=29, show="*", font=('calibri', 20), fg="black")
    r_cpassword_e.place(x=170, y=130)

    r_ph_l = Label(top, text="Phone:", bg="light grey", font=('calibri', 20), width=15, anchor=W)
    r_ph_l.place(x=30, y=180)
    r_ph_e = Entry(top, width=29, font=('calibri', 20), fg="black")
    r_ph_e.place(x=170, y=180)

    button_register = Button(top, text="Register", width=15, height=1, font=('comicsans', 18), command=cheeck)
    button_register.place(x=200, y=250)

    top.mainloop()


root = Tk()
root.geometry("1340x680+10+10")
root.title("LOG IN")

filename = PhotoImage(file="login_wall1_0.4.png")
background_label = Label(root, image=filename, anchor=W)
background_label.place(x=0, y=0, relwidth=1, relheight=1)

# C = Canvas(root, bg='light grey', height=300, width=500)


# C.place(x=800,y=300)
# root.wm_attributes('-transparentcolor', '#ab23ff')
l_e1 = Label(root, text="Username", bg="light grey", font=('calibri', 20), width=15, anchor=W)
l_e1.place(x=850, y=330)
un_e1 = Entry(root, width=29, font=('calibri', 20), fg="black")
un_e1.place(x=850, y=380)

l_p1 = Label(root, text="Password", bg="light grey", font=('calibri', 20), width=15, anchor=W)
l_p1.place(x=850, y=430)
un_p1 = Entry(root, width=29, show="*", font=('calibri', 20), fg="black")
un_p1.place(x=850, y=480)

button_login = Button(root, text="Log In", width=15, height=1, font=('comicsans', 14), command=cheeck_login)
button_login.place(x=850, y=540)
button_signup = Button(root, text="Sign Up", width=15, height=1, font=('comicsans', 14), command=sign_up)
button_signup.place(x=1070, y=540)

root.mainloop()
